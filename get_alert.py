#!/usr/bin/env python
import requests, json, sys, getopt

def Usage():
    print "get_alert.py Usage:"
    print "     python get_alert.py [flags]"
    print "     **Availbale Flags"
    print "     -h : Help about command"
    print "     -t <sysdig token> : Provde Sysdig Cloud token to access API"
    print "     --token <sysdig token> : Provde Sysdig Cloud token to access API"
    print "     -i <alertId> : Show single alerts configurations"
    print "     --alert_id <alertId> : Show single alerts configurations"
    sys.exit()

if len(sys.argv) == 1:
    Usage()
if sys.argv[1] =='-h':
    Usage()

try:
    options, args = getopt.getopt(sys.argv[1:],"t:i:",["token=","alert_id="])
except getopt.GetoptError:
    Usage()

url = 'https://app.sysdigcloud.com/api/alerts'
token_receive=0 #Check whether token is received correctly
for name,value in options:
    if name in ("-t", "-token"):
        token='Bearer '+value
        token_receive=1
    if name in ("-i", "alert_id"):
        url = 'https://app.sysdigcloud.com/api/alerts/'+value

if token_receive==0:
    print "Missing token"
    Usage()

headers = {'Content-Type': 'application/json', 'Accept' : 'application/json', 'Authorization': token}
result = requests.get(url, headers=headers)
if result.status_code==404:
    print "Couldn't find the alert with this ID"
    sys.exit()
print json.dumps(result.json(), indent=4, sort_keys=True)
