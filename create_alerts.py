import requests, json, glob, sys

def Usage():
    print "get_alert.py Usage:"
    print "     python create_alerts.py [flag]"
    print "     **Availbale Flags"
    print "     -h : Help about command"
    print "     -t <sysdig token> : Provde Sysdig Cloud token to access API"
    print "     --token <sysdig token> : Provde Sysdig Cloud token to access API"
    sys.exit()


if sys.argv[1]=='-h':
    Usage()
else:
    if len(sys.argv) != 3:
        print "Incorrect number of input parameters."
        Usage()
    if sys.argv[1] in ['-t','--token']:
        token='Bearer '+sys.argv[2]
        url = 'https://app.sysdigcloud.com/api/alerts'
        headers = {'Content-Type': 'application/json', 'Accept' : 'application/json', 'Authorization' : token}
        file_list=glob.glob('json/*.json')

        for file in file_list:
            contents = open(file, 'rb').read()
            result = requests.post(url, data=contents, headers=headers)

            if result.status_code==200:
                print "SUCCESS: Alert %s is added" % result.json()["alert"]["name"]
            else:
                if result.status_code==422:
                    print "ERROR %d : ( %s) %s" % (result.status_code, json.loads(contents)["alert"]["name"], result.json()["errors"][0]["message"]) #Print the error message
                else:
                    # print "ERROR %d : %s" % (result.status_code, result.json()["errors"][0]["message"]) #Print the error message
                    print "ERROR %d : %s" % (result.status_code, result.text) #Print the error message
                    if result.status_code==401:
                        sys.exit(3)

    else:
        print "Incorrect number of input parameters."
        Usage()


# Status Code Reference:
# 400 invalid alert configuration
# 401 unauthorized access, exit check the token
# 422 alert name is already taken
# 200 OKclear
# default Unexpected error
