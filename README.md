# Sysdig Alerts

Sydig Cloud is the infrastructure level monitoring tool that is currently used in the Bitesize PaaS project. It is designed to monitor system with containers, which provides a good support to   monitor Kubernetes infrastructure.

This README is intended to show you how to use the python scripts to request the Sysdig Cloud API for managing alerts, which will be integrated into the automation process such as Stackstorm.

Sysdig Cloud API reference:  
https://app.sysdigcloud.com/apidocs/


### Sysdig API token:
Your Sysdig Cloud API Token is needed to use the scripts to manage your Sysdig Cloud. The token can be found from Sysdig Web UI.
1. Login to your Sysdig Cloud at https://app.sysdigcloud.com/#/login
2. Go to the setting in the top right corner and copy your Sysdig Cloud API Token.


### Script Usage:
##### 1. Return the existing alerts configuration: get_alerts.py
    python get-alert.py [flags]
    -h : Help about command
    -t <sysdig token> : Provde Sysdig Cloud token to access API
    --token <sysdig token> : Provde Sysdig Cloud token to access API
    -i <alertId> : Show single alerts configurations
    --alert_id <alertId> : Show single alerts configurations

All the alert configurations will be returned if you don't provide "-i" or ""--alert_id" for specific alert ID.

######Sample Usage:

Get the alert ID from Sysdig Cloud Web UI to get the configuration file for that alert.

e.g. 38516 for API - FS used percent

    $ python get_alert.py -t f8757121-34ab-4ad6-93ae-3e06db240a46 -i 38516

    {
    "alert": {
        "condition": "max(fs.used.percent) > 80",
        "createdOn": 1460130129000,
        "description": "API - FS used percent",
        "enabled": true,
        "filter": "agent.tag.environment = \"prod-117\"",
        "id": 38516,
        "modifiedOn": 1460130129000,
        "name": "API - FS used percent",
        "notificationCount": 0,
        "notify": [
            "EMAIL"
        ],
        "segmentBy": [
            "kubernetes.pod.name"
        ],
        "segmentCondition": {
            "type": "ANY"
        },
        "severity": 2,
        "timespan": 300000000,
        "type": "MANUAL",
        "version": 1
    }
    }

##### 1. Create new alerts using Json configuration files: create_alerts.py
    python create_alerts.py -t <sysdig token>
    -h : Help about command
    -t <sysdig token> : Provde Sysdig Cloud token to access API
    --token <sysdig token> : Provde Sysdig Cloud token to access API

All the \*.json files in the json foler will be used to create new alerts on your Sysdig Cloud.

######Sample Usage:

    $ python create_alerts.py -t f8757121-34ab-4ad6-93ae-3e06db240a46

    SUCCESS: Alert API - /mnt/docker volumes used is added
    SUCCESS: Alert API - Consul down is added
    SUCCESS: Alert API - Consul HTTP request time is added
    SUCCESS: Alert API - Consul pods number is added
    SUCCESS: Alert API - Consul Request Time is added
    SUCCESS: Alert API - FS used percent is added
    SUCCESS: Alert API - Nginx Down is added
    SUCCESS: Alert API - Nginx HTTP Request Time is added
    SUCCESS: Alert API - Nginx 443 connection count is added
    SUCCESS: Alert API - Nginx 80 connection count is added
    SUCCESS: Alert API - Nginx Request Time is added
