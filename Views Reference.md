# Sysdig views reference

### Explore Page
https://app.sysdigcloud.com/#/explore

The explore page is the best place to have an overview of the infrastructure and add alerts and dashboard panels. By setting the grouping, a good views can be obtained. After selecting the namespce/node, some views option can be found at the bottom left corner to get different views.

####1. Recommand explore grouping (In order)
  agent.tag.environment
  --kubernetes.namespace.name
  --kubernetes.service.name
  --kubernetes.pod.name

  ***

####2. Common View Options
  * System - Container Overview:
    * CPU % by Process
    * Memory Usage by Process
    * Network Traffic by Protocol
    * File I/O Bytes by Process
    * Number of Application Requests
    * Average and Worst Application Request Time    
  * System - File System

  * System - Memory
    * Memory Statistics: Avg. Resident Memory, Avg. Memory Usage, etc.
    * Memory Usage Over Time
    * Page Faults Over Time
    * Memory Usage - Top Processes
    * Page Faults - Top Processes
    * Memory Usage - Top Hosts
    * Page Faults - Top Hosts   
  * System - Overview by Processes
    * Process Statistics: CPU %
    * Memory Usage %
    * Network Bytes
    * Network Connections
    * Disk Usage
    * File Bytes
  * Network - Connections table
  * Network - Overview
     Network Total/In/Out
  * Topology - CPU Usage
  * Topology - Network Traffic
  * Topology - Response Times (Useful!)

***

####3. Common Metric Views Options
  By selecting the metric with the suitable time period, a panel showing the value for that metric in that period of time will be generated. Options are provided to get a better view:

  * Aggregation: Average, Maximum, Minimum, Sum
  * Segment by: kubernetes.pod.name (Recommanded)
  * Time period: Adjust on the top right of the Page

  #####Common Metric:
  * #####Network
    * net.request.time
    * net.http.request.time
    * container.count
    * net.bytes.(in/out/total)
    * net.connection.count.(in/out/total)
  * #####System
    * uptime: Monitor pod entity down
    * fs.(free/used).percent
    * fs.bytes.(free/total/used)
    * memory.used.percent
    * memory.bytes.(total/used/swap)
    * CPU.used.percent

***
####4. How to store the views to dashboard?
  Using the pin button in the top right of all of the view panels, views and metric views can be easily pin into a dashboard so that a customized views of diagrams can be used to visualize your system as a dashboard once you log into Sysdig Cloud interface.
